from channels.routing import route_class
from app.consumers import MyConsumer

channel_routing = [
    route_class(MyConsumer)
]
