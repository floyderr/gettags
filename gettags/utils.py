from django.core.cache import cache
import hashlib
import re
from collections import Counter
import time
from facebook import GraphAPI
from PIL import Image, ImageDraw, ImageFont
from social.apps.django_app.default.models import UserSocialAuth


# get the cache key for storage
def cache_get_key(*args, **kwargs):
    serialise = []
    for arg in args:
        serialise.append(str(arg))
    for key, arg in kwargs.items():
        serialise.append(str(key))
        serialise.append(str(arg))
    full_str = u"".join(serialise).encode('utf-8')
    key = hashlib.md5(full_str).hexdigest()
    return key


# decorator for caching functions
def cache_for(time):
    def decorator(fn):
        def wrapper(*args, **kwargs):
            key = cache_get_key(fn.__name__, *args, **kwargs)
            result = cache.get(key)
            if not result:
                result = fn(*args, **kwargs)
                cache.set(key, result, time)
            return result

        return wrapper

    return decorator


def count_hashtags(posts):
    hashtags_counter = Counter()
    for post in posts['data']:
        message = post.get('message')
        if message:
            tags = re.findall(r'#(\w+)', message)
            for x in tags:
                hashtags_counter[x] += 1
    return hashtags_counter


def get_hashtag_list_for_user(user, count):
    try:
        social = user.social_auth.get(provider='facebook')
        token = social.extra_data['access_token']
        user_id = social.extra_data['id']
        graph = GraphAPI(token)
        posts = graph.get_connections(user_id, 'feed')
        hashtags_counter = count_hashtags(posts)
        hashtags = [x[0] for x in hashtags_counter.most_common(count)]
    except (UserSocialAuth.DoesNotExist, AttributeError):
        hashtags = []

    return hashtags


@cache_for(10)
def create_image_from_hashtags(hashtags):
    image = Image.new("RGBA", [320, 150])
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype("ARIALUNI.TTF", 20)
    for x, i in enumerate(hashtags):
        draw.text((100, x * 20), i, (255, 255, 155), font=font)
    draw.text((55, 120), str(time.ctime()), (255, 255, 155), font=font)
    return image
