from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

from app import views as app_views

urlpatterns = [
    url(r'^$', app_views.home, name='home', ),
    url(r'^logout/', auth_views.logout,  {'next_page': '/'}, name='logout', ),
    url(r'^oauth/', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', admin.site.urls),
]
