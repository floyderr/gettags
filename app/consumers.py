import base64
import json

import time
from channels.generic.websockets import WebsocketConsumer
from gettags.utils import get_hashtag_list_for_user, create_image_from_hashtags


class MyConsumer(WebsocketConsumer):
    http_user = True

    def receive(self, text=None, bytes=None, **kwargs):
        hashtag_list = get_hashtag_list_for_user(user=self.message.user, count=5)
        image = create_image_from_hashtags(hashtag_list)
        img = image
        img.save("img2.png", "PNG")
        with open("img2.png", "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
        self.send(text=json.dumps({
            'image': encoded_string.decode('utf8'),
            'time': str(time.ctime())
        }))
